set(TARGET lomiri-download-manager-common)

set(SOURCES
	lomiri/download_manager/download_state_struct.cpp
	lomiri/download_manager/download_struct.cpp
	lomiri/download_manager/group_download_struct.cpp
	lomiri/download_manager/system/logger.cpp
)

set(PUBLIC_HEADERS
	lomiri/download_manager/download_state_struct.h
	lomiri/download_manager/download_struct.h
	lomiri/download_manager/group_download_struct.h
	lomiri/download_manager/metatypes.h
)

set(PRIVATE_HEADERS
	lomiri/download_manager/system/logger.h
)

include_directories(${Qt5DBus_INCLUDE_DIRS})
include_directories(${Qt5Network_INCLUDE_DIRS})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${CMAKE_SOURCE_DIR}/src/common/public)

add_library(${TARGET} SHARED 
	${SOURCES}
	${PUBLIC_HEADERS}
	${PRIVATE_HEADERS}
)


set(symbol_map "${CMAKE_SOURCE_DIR}/src/downloads/common/symbols.map")
set_target_properties(
	${TARGET}

	PROPERTIES
        LINK_FLAGS "${ldflags} -Wl,--version-script,${symbol_map}"
        LINK_DEPENDS ${symbol_map}
	VERSION ${LDM_VERSION_MAJOR}.${LDM_VERSION_MINOR}.${LDM_VERSION_PATCH}
	SOVERSION ${LDM_VERSION_MAJOR}
)

target_link_libraries(${TARGET}
	${Qt5DBus_LIBRARIES}
	ldm-common
)

configure_file(${TARGET}.pc.in ${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.pc @ONLY)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.pc DESTINATION ${CMAKE_INSTALL_LIBEXECDIR}/pkgconfig)
install(TARGETS ${TARGET} DESTINATION ${CMAKE_INSTALL_LIBDIR})
install(FILES ${PUBLIC_HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/lomiri/download_manager)
